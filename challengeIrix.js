(function () {
    var ANALYZE_1 = "Q1: What are the time (Big O notation) and space complexity of your solution?";
    var A1 = "A1: My solution has a complexity of O(N-((N/2)+1)) because I proved with different cases of sequences and it was the most suitable formula for the algorithm"
    var ANALYZE_2 = "Q2: What are the limitations of your solution?";
    var A2 = "A2: There are no limitations for my solution, it can analize any sequenc, no matter the length, and it has all the validations"
    var ANALYZE_3 = "Q3: Why have you decided to implement it on that way?";
    var A3 = "A3: Because this way was the most suitable to compare each number between the others avoiding the repetition and unnecesary extra loops"
    var POSSIBLE_IMPROVEMENTS = "Q4: What do you think we could do to get a better implementation?";
    var A4 = "A4: A possible improvement could be the storage of numbers already processed, to avoid loops of comparisson of numbers that has been determined before as a repeated number"
    
    // Variable para la secuencia de números
    var sequence = [];

    // Método principal donde se ejecuta todo el programa
    main();

    /**
        Método para agregar números a la secuencia.
        Recibe number de tipo entero
        - Tiene la validación de que si llega un valor diferente a un entero, no lo admite y no lo agrega al arreglo
    **/
    function add(number) {
        if (number === parseInt(number, 10)) {
            sequence.push(number);
            console.info("The number " + number + " has been successfully added")
        } else {
            console.info("The value " + number + " is not a valid number")
        }
    }

    /**
        Método que determina cual es el primer número que no está repetido en la secuencia de izquierda a derecha
        Variables:
            
            state = actúa como un acumulador booleano para determinar si encontró o no repetidos
                    termina en falso si encontró un repetido
                    termina en verdadero si todos resultaron distintos
            nonRepeated = es la variable que almacena el primer número no repetido que encontró

        - Tiene un comparador de indices con un continue que evade comparar el número en cuestión con sí mismo
        - Tiene un condicional que determina si ya encontró un repetido por medio de un acumulador, en ese caso 
        debe pasar al siguiente número
        - Al final de los ciclos del segundo loop, se determina si state sigue en verdadero, si ese es el caso
        es entonces porque no se encontró ningún repetido con los otros número de la secuencia
        - Al final, se muestra un mensaje diciendo cual es el primer número que no está repetido
    **/
    function getFirstNonDuplicated() {
        var state = true;
        var nonRepeated = "";

        for (var i = 0; i < sequence.length; i++) {
            state = true;
            for (var o = 0; o < sequence.length; o++) {
                if (o == i) {
                    continue;
                }
                state = state && (sequence[i] != sequence[o]);
                if (state === false) {
                    break;
                }
            }
            if (state === true) {
                nonRepeated = sequence[i];
                break;
            } else {
                state = true;
            }
        }

        if (nonRepeated === "") {
            console.info("All numbers of the sequence are repeated");
        } else {
            console.info("The first non-repeated number of the sequence is " + nonRepeated);
        }

    }

    /**
        Método en el que respondó cada pregunta del ejercicio
    **/
    function analizeQuestions() {
        console.info("");
        console.info(ANALYZE_1);
        console.info(A1);
        console.info("");
        console.info(ANALYZE_2);
        console.info(A2);
        console.info("");
        console.info(ANALYZE_3);
        console.info(A3);
        console.info("");
        console.info(POSSIBLE_IMPROVEMENTS);
        console.info(A4);
    }

    /**
        Método en el que se ejecuta todo el programa
    **/
    function main() {
        // Creamos los números para las secuencias
        add(6);
        add(6);
        add(5);
        add(5);
        add(5);
        add(4);
        add(3);
        add(9);
        add(2);
        add(8);
        // Al final se imprime el primer número que no está duplicado de izquierda a derecha
        getFirstNonDuplicated();
        // Respuestas a las preguntas que me han hecho
        analizeQuestions();
    }
}());